class CreateAccounts < ActiveRecord::Migration[5.1]
  def change
    create_table :accounts do |t|
      t.string :account_name, null: true
      t.integer :amount, default: 0
      t.string :currency
      t.text :description
      t.integer :admin_id
      t.boolean :status, default: true

      t.timestamps
    end
  end
end

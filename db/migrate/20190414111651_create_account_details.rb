class CreateAccountDetails < ActiveRecord::Migration[5.1]
  def change
    create_table :account_details do |t|
      t.integer :account_id
      t.integer :income_id
      t.integer :total

      t.timestamps
    end
  end
end

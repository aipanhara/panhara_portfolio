class CreatePortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :portfolios do |t|
      t.string :image
      t.integer :category_portfolio_id

      t.timestamps
    end
  end
end

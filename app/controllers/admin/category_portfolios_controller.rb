class Admin::CategoryPortfoliosController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]

  def index
    @category_portfolio= current_admin.category_portfolios.build
    @category_portfolios = CategoryPortfolio.all
  end

  def show
    @category_portfolio= CategoryPortfolio.find(params[:id])
  end

  def new

  end

  def create
    @category_portfolio= current_admin.category_portfolios.build(category_portfolio_params)
    if @category_portfolio.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_category_portfolios_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_category_portfolios_path }
        format.js { render template: "admin/category_portfolios/category_portfolio_error.js.erb" }
      end
    end
  end

  def edit
    @category_portfolio= CategoryPortfolio.find(params[:id])
  end

  def update
    @category_portfolio= CategoryPortfolio.find(params[:id])
    if @category_portfolio.update(category_portfolio_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_category_portfolios_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_category_portfolios_path }
        format.js { render template: "admin/categories/category_error.js.erb" }
      end
    end
  end

  def destroy
    @category_portfolio= CategoryPortfolio.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_category_portfolios_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def category_portfolio_params
      params.required(:category_portfolio).permit!
    end
end

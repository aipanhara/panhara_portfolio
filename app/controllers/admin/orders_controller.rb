class Admin::OrdersController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]

  def invoice
    @invoice = Order.find(params[:id])
  end

  def index
    @order = current_admin.orders.build
    @orders = Order.all.order(id: :desc)
  end

  def show
    @order = Order.find(params[:id])
  end

  def new

  end

  def create
    @order = current_admin.orders.build(order_params)
    if @order.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_orders_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or order Duplicated"
        format.html { redirect_to admin_orders_path }
        format.js { render template: "admin/categories/order_error.js.erb" }
      end
    end
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    if @order.update(order_params)
      @order.line_items.each do |item|
        if @order.status == true
          item.product.update_columns(remaining_stock_quantity: item.product.remaining_stock_quantity - item.quantity)
        end
      end
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_orders_path }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @order = Order.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_orders_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def order_params
      params.required(:order).permit!
    end
end

class CreateExpenses < ActiveRecord::Migration[5.1]
  def change
    create_table :expenses do |t|
      t.integer :amount, default: 0
      t.date :expense_date
      t.text :description
      t.integer :admin_id
      t.string :category
      t.integer :account_id
      t.text :description
      t.timestamps
    end
  end
end

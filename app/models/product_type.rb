class ProductType < ApplicationRecord
  before_save :update_slug
  has_many :products
  belongs_to :admin

  def update_slug
    self.slug = type_name.parameterize
  end

end

class Admin::PortfoliosController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]

  def index
    @portfolio= current_admin.portfolios.build
    @portfolios = Portfolio.all
  end

  def show
    @portfolio= Portfolio.find(params[:id])
  end

  def new

  end

  def create
    @portfolio= current_admin.portfolios.build(portfolio_params)
    if @portfolio.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_portfolios_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_portfolios_path }
        format.js { render template: "admin/portfolios/portfolio_error.js.erb" }
      end
    end
  end

  def edit
    @portfolio= Portfolio.find(params[:id])
  end

  def update
    @portfolio= Portfolio.find(params[:id])
    if @portfolio.update(portfolio_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_portfolios_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_portfolios_path }
        format.js { render template: "admin/categories/category_error.js.erb" }
      end
    end
  end

  def destroy
    @portfolio= Portfolio.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_portfolios_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def portfolio_params
      params.required(:portfolio).permit!
    end
end

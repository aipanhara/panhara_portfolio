class AddRemainingStockQuantityToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :remaining_stock_quantity, :integer
  end
end

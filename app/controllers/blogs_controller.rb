class BlogsController < FrontEndApplication
  def index
    # if the id params is present
    if params[:id]
      # get all records with id less than 'our last id'
      # and limit the results to 5
      @search = Blog.where('id < ?', params[:id]).limit(6).order(id: :desc).ransack(params[:search])
      @blogs = @search.result.includes(:category)
    else
      @search = Blog.limit(6).order(id: :desc).ransack(params[:search])
      @blogs = @search.result.includes(:category)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @blogs = Blog.all.order(id: :desc).limit(8)
    @blog = Blog.find_by_slug(params[:id])
  end

end

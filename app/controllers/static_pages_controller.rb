class StaticPagesController < FrontEndApplication
  def home
    if user_signed_in?
      @current_user = current_user
    end
    @search = Product.ransack(params[:search])
    @products = @search.result
    @categories = Category.all
    @galleries = Gallery.all
  end


  def about
    @categories = Category.all
   end

  def contact
    @categories = Category.all
  end

  def term
      @categories = Category.all
   end

  def faq
      @categories = Category.all
    end

  def privacy
    @categories = Category.all
    end

  def guide
    @categories = Category.all
    end

  def delivery
    @categories = Category.all
  end

  def under_construction

  end
end

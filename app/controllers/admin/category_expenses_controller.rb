class Admin::CategoryExpensesController < ApplicationController

  before_action :authenticate_admin!
  before_action :admin_only?

  def index
    @category_expense= current_admin.category_expenses.build
    @category_expenses = CategoryExpense.all
  end

  def show
    @category_expenses= CategoryExpense.find(params[:id])
  end

  def new

  end

  def create
    @category_expense= current_admin.category_expenses.build(category_expense_params)
    if @category_expense.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_category_expenses_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or category_expensesDuplicated"
        format.html { redirect_to admin_category_expenses_path }
        format.js { render template: "admin/category_expenses/category_expense_error.js.erb" }
      end
    end
  end

  def edit
    @category_expense= CategoryExpense.find(params[:id])
  end

  def update
    @category_expense= CategoryExpense.find(params[:id])
    if @category_expense.update(category_expense_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_category_expenses_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_category_expenses_path }
        format.js { render template: "admin/category_expenses/category_expense_error.js.erb" }
      end
    end
  end

  def destroy
    @category_expense= CategoryExpense.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_category_expenses_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end


    def category_expense_params
      params.required(:category_expense).permit!
    end


end

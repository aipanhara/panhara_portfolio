class Admin::IncomesController < ApplicationController

  before_action :authenticate_admin!
  before_action :admin_only?

  def index
    @income= current_admin.incomes.build
    @incomes = Income.all

  end

  def show
    @incomes= Income.find(params[:id])
  end

  def new

  end

  def create
    @income= current_admin.incomes.build(income_params)
    if @income.save
      @account = @income.account
      AccountDetail.create!(
        income_id: @income.id,
        account_id: @account.id,
        total: @income.amount + Account.sum(:amount)
      )
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_incomes_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or incomesDuplicated"
        format.html { redirect_to admin_incomes_path }
        format.js { render template: "admin/incomes/income_error.js.erb" }
      end
    end
  end

  def edit
    @income= Income.find(params[:id])
  end

  def update
    @income= Income.find(params[:id])
    if @income.update(income_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_incomes_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_incomes_path }
        format.js { render template: "admin/incomes/income_error.js.erb" }
      end
    end
  end

  def destroy
    @income= Income.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_incomes_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end


    def income_params
      params.required(:income).permit!
    end


end

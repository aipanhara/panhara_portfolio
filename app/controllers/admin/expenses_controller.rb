class Admin::ExpensesController < ApplicationController

  before_action :authenticate_admin!
  before_action :admin_only?

  def index
    @expense = current_admin.expenses.build
    @expenses = Expense.all
  end

  def show
    @expenses= Expense.find(params[:id])
  end

  def new

  end

  def create
    @expense= current_admin.expenses.build(expense_params)
    if @expense.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_expenses_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or expensesDuplicated"
        format.html { redirect_to admin_expenses_path }
        format.js { render template: "admin/expenses/expense_error.js.erb" }
      end
    end
  end

  def edit
    @expense= Expense.find(params[:id])
  end

  def update
    @expense= Expense.find(params[:id])
    if @expense.update(expense_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_expenses_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_expenses_path }
        format.js { render template: "admin/expenses/expense_error.js.erb" }
      end
    end
  end

  def destroy
    @expense= Expense.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_expenses_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end


    def expense_params
      params.required(:expense).permit!
    end


end

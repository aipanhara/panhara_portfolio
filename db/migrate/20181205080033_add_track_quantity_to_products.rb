class AddTrackQuantityToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :track_quantity, :integer
  end
end

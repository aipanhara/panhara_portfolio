class CreateIncomes < ActiveRecord::Migration[5.1]
  def change
    create_table :incomes do |t|
      t.integer :amount, default: 0
      t.text :description
      t.integer :admin_id
      t.string :category
      t.integer :account_id
      t.timestamps
    end
  end
end

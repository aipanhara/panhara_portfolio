class AddAdminIdToPortfolios < ActiveRecord::Migration[5.1]
  def change
    add_column :portfolios, :admin_id, :integer
  end
end

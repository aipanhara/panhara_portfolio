class Admin::IncomeAndExpenseDashboardsController < ApplicationController
  def index
    @incomes = Income.sum(:amount)
    @expenses = Expense.sum(:amount)
    @accounts = Account.sum(:amount)
    @account_details = AccountDetail.sum(:total)
    @finale_total = @account_details - @expenses
  end
end

Rails.application.routes.draw do

    root 'home#index'

    devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: "users/registrations"
    }

    # devise_for :admins, :controllers => { :registrations => :registrations }
    devise_for :admins, :skip => [:registrations]

    devise_scope :admin do
      get "/signin" => "devise/sessions#new", :as => "signin" # custom path to login/sign_in
    end

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new", :as => "sign_in" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", :as => "sign_up" # custom path to login/sign_in
      get "users/verify/" => 'users/registrations#show_verify', as: 'verify'
      post "users/verify"
      post "users/resend"
    end

    as :admin do
      get 'admins/edit' => 'devise/registrations#edit', :as => 'edit_admin_registration'
      put 'admins' => 'devise/registrations#update', :as => 'admin_registration'
      patch 'admins' => 'devise/registrations#update', :as => 'update_admin_registration'
    end


    namespace :admin do
      root "dashboards#index"
      get 'dashboards' => "dashboards#index", :as => "dashboard"
      resources :portfolios
      resources :categories
      resources :blogs
      resources :admins
      # Sale
      resources :category_portfolios
      resources :products
      resources :product_types
      resources :galleries
      resources :orders
      resources :line_items
      resources :settings, only: [:new, :create, :edit, :update]
      resources :users
      resources :settings, only: [:new, :create, :edit, :update]
      resources :incomes
      resources :expenses
      resources :accounts
      resources :income_and_expense_dashboards
    end

    resources :blogs

    resources :wishlists
    resources :products do
      resources :reviews
    end

    resources :orders
    resources :product_types, only: [:index, :show]
    get 'profiles' => "orders#profile", :as => "profiles"
    get 'carts/:id' => 'carts#show', as: "cart"
    delete 'carts/:id' => 'carts#destroy'

    post 'line_items/:id/add' => 'line_items#add_quantity', as: 'line_item_add'
    post 'line_items/:id/reduce' => 'line_items#reduce_quantity', as: 'line_item_reduce'
    post 'line_items/:id/s_size'=> 'line_items#s_size', as: 's_size'
    post 'line_items/:id/m_size'=> 'line_items#m_size', as: 'm_size'
    post 'line_items/:id/l_size'=> 'line_items#l_size', as: 'l_size'
    post 'line_items/:id/xl_size'=> 'line_items#xl_size', as: 'xl_size'
    post 'line_items' => 'line_items#create'
    get 'line_items/:id' => 'line_items#shiow', as: "line_item"
    delete 'line_items/:id' => 'line_items#destroy'

    post 'wishlist_line_items/:id/add' => 'wishlist_line_items#add_quantity', as: 'wishlist_line_item_add'
    post 'wishlist_line_items/:id/reduce' => 'wishlist_line_items#reduce_quantity', as: 'wishlist_line_item_reduce'
    post 'wishlist_line_items' => 'wishlist_line_items#create'
    get 'wishlist_line_items/:id' => 'wishlist_line_items#shiow', as: "wishlist_line_item"
    delete 'wishlist_line_items/:id' => 'wishlist_line_items#destroy'

    get 'users' => 'users#index', :as => "users"
    post 'users' => "users#create"
    get 'signup' => "users#new", :as => "new_user"
    get "users/:id/edit" => "users#edit", :as => "edit_user"
    get "users/:id" => "users#show", :as => "user"
    patch "users/:id" => "users#update"
    put "users/:id" => "users#update"
    delete "users/:id" => "users#destroy"

    get 'under_constructions' => "static_pages#under_construction", as: "under_construction"
    get 'profile' => "orders#profile", as: "profile"
    resources :product_types
end

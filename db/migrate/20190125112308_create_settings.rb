class CreateSettings < ActiveRecord::Migration[5.1]
  def change
    create_table :settings do |t|
      t.boolean :maintain
      t.integer :admin_id

      t.timestamps
    end
  end
end

class LineItemsController < FrontEndApplication
  # before_action :authenticate_user!, only: [:create, :destroy, :add_quantity, :reduce_quantity]
  def create
    chosen_product = Product.find(params[:product_id])
    add_items_to_cart(chosen_product)
    respond_to do |format|
      if @line_item.save!
        flash.now[:notice] = "#{@line_item.product.product_name} Added To Cart"
        format.html { redirect_back(fallback_location: @current_cart)}
        format.js
      else
        format.html { render :new }
        flash.now[:alert] = "Please fill the field blank or product Duplicated"
        format.js { render template: "line_items/line_item_error.js.erb" }
      end
    end
  end

  def destroy
    @cart = @current_cart
    @line_item = LineItem.destroy(params[:id])
    respond_to do |format|
      flash.now[:alert] = "#{@line_item.product.product_name}  was removed from cart!"
      format.html { redirect_to @current_cart }
      format.js
    end
  end

  def s_size
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    @line_item.update(shirt_size: "S") if params[:shirt_size] == "S"
  end

  def m_size
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    @line_item.update(shirt_size: "M") if params[:shirt_size] == "M"
  end

  def l_size
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    @line_item.update(shirt_size: "L") if params[:shirt_size] == "L"
  end

  def xl_size
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    @line_item.update(shirt_size: "XL") if params[:shirt_size] == "XL"
  end

  def add_quantity
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    if @line_item.product.remaining_stock_quantity > @line_item.quantity
      @line_item.quantity += 1
    else
      respond_to do |format|
        flash.now[:alert] = "Out Of Stock"
        format.js { render template: "/line_items/out_of_stock_error.js.erb" }
      end
    end

    if @line_item.save
      respond_to do |format|
        format.html { redirect_back(fallback_location: @current_cart)}
        format.js
      end
    end
  end

  def reduce_quantity
    @cart = @current_cart
    @line_item = LineItem.find(params[:id])
    if @line_item.quantity > 1
      @line_item.quantity -= 1
      if @line_item.save
        respond_to do |format|
          format.html { redirect_back(fallback_location: @current_cart)}
          format.js
        end
      end
    elsif @line_item.quantity = 1
      respond_to do |format|
        flash.now[:alert] = "You can`t do that!"
        format.html { redirect_back(fallback_location: @current_cart)}
        format.js { render template: "/line_items/minus_one.js.erb" }
      end
    end
  end

  private
    def line_item_params
      params.require(:line_item).permit(:id,:quantity, :product_id, :cart_id, :order_id, :user_id, :size_m)
    end

    def add_items_to_cart(chosen_product)
      if @current_cart.products.include?(chosen_product)
        @line_item = @current_cart.line_items.find_by(product_id: chosen_product)
        @line_item.quantity += 1
        # @line_item.update(user_id: current_user.id)
      else
        @line_item = LineItem.new
        @line_item.cart = @current_cart
        @line_item.product = chosen_product
        @line_item.quantity = 1
        # @line_item.update(user_id: current_user.id)
      end
    end

end

class Admin::AccountsController < ApplicationController

  before_action :authenticate_admin!
  before_action :admin_only?

  def index
    @account = current_admin.accounts.build
    @accounts = Account.all
    @total_revenues = Account.joins(:account_details).sum(:total)
    @total_expense = Expense.sum(:amount)
    @finale_total = @total_revenues - @total_expense
  end

  def show
    @account = Account.find(params[:id])
  end

  def new

  end

  def create
    @account = current_admin.accounts.build(account_params)
    if @account.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_accounts_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or account Duplicated"
        format.html { redirect_to admin_accounts_path }
        format.js { render template: "admin/accounts/account_error.js.erb" }
      end
    end
  end

  def edit
    @account = Account.find(params[:id])
  end

  def update
    @account = Account.find(params[:id])
    if @account.update(account_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_accounts_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_accounts_path }
        format.js { render template: "admin/accounts/account_error.js.erb" }
      end
    end
  end

  def destroy
    @account = Account.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_accounts_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def account_params
      params.required(:account).permit!
    end
end

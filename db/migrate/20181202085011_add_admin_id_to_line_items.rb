class AddAdminIdToLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :line_items, :admin_id, :integer
  end
end

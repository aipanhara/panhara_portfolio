class OrdersController < FrontEndApplication
  before_action :authenticate_user!, only: [:index, :show, :new, :create, :profile]
  before_action :cart_is_empty, only: [:new, :create]
  before_action :isBlock?, only: [:create]
  def profile
    @orders = current_user.orders.all
    @categories = Category.all
  end

  def index
    @orders = Order.all
  end

  def show
    @order = Order.find(params[:id])
  end

  def new

    @order = Order.new
    @cart = @current_cart
  end

  def create
    @order = Order.new(order_params)
    @order.update(user_id: current_user.id)
    client = Slack::Web::Client.new
    client.auth_test
    if @order.save
      add_line_items_to_order
      reset_sessions_cart
      redirect_to profile_path
      flash[:notice] = "Thank for your order"
      client.chat_postMessage(
        channel: '#mybusiness',
        text: "http://www.panhara.work/admin/orders/#{@order.id}/edit",
        as_user: true
      )
    else
      flash[:alert] = "Please fill the bill infomation"
      render :new
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to orders_path
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    @order.update(order_params)
    redirect_to orders_path
  end

  def cart_is_empty
    return unless @current_cart.line_items.empty?
    flash[:alert] = "You cart is empty"
    redirect_to cart_path(@current_cart)
  end

  private
    def add_line_items_to_order
      @current_cart.line_items.each do |item|
        item.cart_id = nil
        item.order_id = @order.id
        item.save
        @order.line_items << item
      end
    end

    def reset_sessions_cart
      Cart.destroy(session[:cart_id])
      session[:cart_id] = nil
    end

    def isBlock?
      if current_user.isBlock == true
        redirect_to root_path, alert: "Your Account Was Blocked By Administrator Contact This Number 095477325"
      end
    end

    def order_params
      params.require(:order).permit(:user_id, :first_name, :last_name, :address_one, :address_two, :payment, :country, line_items_attributes:[:id, :product_id])
    end
end

class CartsController < FrontEndApplication
  before_action :authenticate_user!, only: [:show, :destroy]
  before_action :isBlock?, only: [:show]
  def show
    @search = Product.ransack(params[:search])
    @products = @search.result
    @cart = @current_cart
  end

  def destroy
    @cart = @current_cart
    @cart.destroy
    session[:cart_id] = nil
    redirect_to root_path
  end

  private
    def cart_params
      params.require(:cart).permit(:user_id)
    end

    def isBlock?
      if current_user.isBlock == true
        redirect_to root_path, alert: "Your Account Was Blocked By Administrator Contact This Number 095477325"
      end
    end
end

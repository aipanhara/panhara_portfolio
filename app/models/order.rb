class Order < ApplicationRecord
  has_many :line_items, dependent: :destroy
  belongs_to :user, optional: true
  belongs_to :admin, optional: true
  has_many :wishlist_line_items, dependent: :destroy
  validates :first_name, :last_name, :address_one, presence: true
  validates :first_name, :last_name, length: {maximum: 15}

  def sub_total
    sum = 0
    line_items.each do |line_item|
      sum += line_item.total_price
    end
    sum
  end


end

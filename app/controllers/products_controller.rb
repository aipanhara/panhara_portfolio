class ProductsController < FrontEndApplication
  before_action :settings?

  def index
    # if the id params is present
    if params[:id]
      # get all records with id less than 'our last id'
      # and limit the results to 5
      @search = Product.where('id < ?', params[:id]).limit(6).order(id: :desc).ransack(params[:search])
      @products = @search.result.includes(:product_type)
    else
      @search = Product.limit(6).order(id: :desc).ransack(params[:search])
      @products = @search.result.includes(:product_type)
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show
    @product = Product.find_by_slug(params[:id])
  end

  private
    def settings?
      if Setting.exists?
        if Setting.first.maintain == true
          redirect_to under_construction_path
        else
          render :index
        end
      end
    end
end

class AddShirtSizeToLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :line_items, :shirt_size, :string
  end
end

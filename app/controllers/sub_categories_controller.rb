class SubCategoriesController < FrontEndApplication
  def index

  end

  def show
    @subcategory = SubCategory.find_by_slug(params[:id])
  end
end

class WishlistsController < FrontEndApplication
  before_action :authenticate_user!, only: [:show, :destroy]
  before_action :isBlock?

  def show
    @wishlist = @current_wishlist
  end

  def destroy
    @wishlist = @current_wishlist
    @wishlist.destroy
    session[:wishlist_id] = nil
    redirect_to root_path
  end

  private
    def wishlist_params
      params.require(:wishlist).permit(:user_id)
    end

    def isBlock?
      if current_user.isBlock == true
        redirect_to root_path, alert: "Your Account Was Blocked By Administrator Contact This Number 095477325"
      end
    end
end

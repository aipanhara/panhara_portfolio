class Category < ApplicationRecord
  before_save :update_slug
  belongs_to :admin
  has_many :blogs

  def update_slug
    self.slug = category_name.parameterize
  end

end

class Admin::TypeIncomesController < ApplicationController

  before_action :authenticate_admin!
  before_action :admin_only?

  def index
    @type_income = current_admin.type_incomes.build
    @type_incomes = TypeIncome.all
  end

  def show
    @type_income = TypeIncome.find(params[:id])
  end

  def new

  end

  def create
    @type_income = current_admin.type_incomes.build(type_income_params)
    if @type_income.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_type_incomes_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or type_income Duplicated"
        format.html { redirect_to admin_type_incomes_path }
        format.js { render template: "admin/type_incomes/type_income_error.js.erb" }
      end
    end
  end

  def edit
    @type_income = TypeIncome.find(params[:id])
  end

  def update
    @type_income = TypeIncome.find(params[:id])
    if @type_income.update(type_income_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_type_incomes_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_type_incomes_path }
        format.js { render template: "admin/type_incomes/type_income_error.js.erb" }
      end
    end
  end

  def destroy
    @type_income = TypeIncome.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_type_incomes_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end


    def type_income_params
      params.required(:type_income).permit!
    end


end

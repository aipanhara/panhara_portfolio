class AddFieldsToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :first_name, :string
    add_column :orders, :last_name, :string
    add_column :orders, :address_one, :string
    add_column :orders, :address_two, :string
    add_column :orders, :payment, :string
    add_column :orders, :country, :string
  end
end

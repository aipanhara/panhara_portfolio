class Blog < ApplicationRecord
  before_save :update_slug
  belongs_to :admin
  belongs_to :category
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  def update_slug
    self.slug = title.parameterize
  end

end

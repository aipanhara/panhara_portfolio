class HomeController < FrontEndApplication

  def index
    @blogs = Blog.all.order(id: :desc).limit(6)
    @portfolios = Portfolio.all.order(id: :desc)
    @portfolio_categories = CategoryPortfolio.all.order(id: :desc)
  end
end

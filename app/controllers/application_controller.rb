class ApplicationController < ActionController::Base
  layout "application"
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_locale
  before_action :set_cart
  before_action :set_wishlist
  before_action :search_product
  # respond_to :html, :xml, :json, :js

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:phone_number,:country_code,:role,:email,:password])
    devise_parameter_sanitizer.permit(:account_update, keys: [:phone_number, :role, :email,:password,:password_confirmation,:current_password])
  end

  def after_sign_out_path_for(resource_or_scope)
    session[:login_url] || request.referer || root_path
  end

  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end

  def store_current_location
    session['user_return_to'] = request.url
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(Admin)
      if admin_signed_in?
        admin_dashboard_path
      else
        root_path
      end
    else
      super
    end
  end

  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end

  def set_locale
    locale = I18n.available_locales.include?(params[:locale].to_sym) ? params[:locale] : I18n.locale if params[:locale].present?
    I18n.locale = locale || I18n.locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge(options)
  end
  private

    def set_cart
        if session[:cart_id]
            cart = Cart.find_by(id: session[:cart_id])
            if cart.present?
              @current_cart = cart
              if current_user
                cart.update(user_id: current_user.id)
              end
            else
              session[:cart_id] = nil
            end
          end

            if session[:cart_id].nil?
              if current_user
                @current_cart = Cart.create(:user_id => current_user.id)
                session[:cart_id] = @current_cart.id
              end
              @current_cart = Cart.create(:user_id => nil)
              session[:cart_id] = @current_cart.id
            end
       end

        def set_wishlist
          if session[:wishlist_id]
            wishlist = Wishlist.find_by(id: session[:wishlist_id])
            if wishlist.present?
              @current_wishlist = wishlist
              if current_user
                wishlist.update(user_id: current_user.id)
              end
            else
              session[:wishlist_id] = nil
            end
          end

          if session[:wishlist_id].nil?
            if current_user
              @current_wishlist = Wishlist.create(:user_id => current_user.id)
              session[:wishlist_id] = @current_wishlist.id
            end
            @current_wishlist = Wishlist.create(:user_id => nil)
            session[:wishlist_id] = @current_wishlist.id
          end
        end

        def search_product
          @cart = @current_cart
        end

        def time_ago(time)
          time
        end
end

class Account < ApplicationRecord
      
  belongs_to :admin
  has_many :incomes
  has_many :account_details
  has_many :expenses

  validates :amount, numericality: { equal_to: 0 }

  def account_total
    sum = 0
    Account.all.each do |account|
      sum += account.amount
    end
    sum
  end

  def account_total_plus_income
    sum = 0
    Account.all.each do |account|
      sum += account.amount
      account.incomes.each do |income|
        sum = sum + income.amount
      end
    end
    sum
  end

  SELECT_CURRENCY = [
    "USD",
    "Riel"
  ]

end

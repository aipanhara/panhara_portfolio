class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  after_initialize :set_default_role, :if => :new_record?

  attr_writer :login

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, authentication_keys: [:login]

  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, :multiline => true
  validates :phone_number, uniqueness: { case_sensitive: false }
  validates :phone_number, presence: true
  validates :phone_number, numericality: true
  has_many :orders
  belongs_to :admin, optional: true
  def active_for_authentication?
    super and self.is_active?
  end

  def login
    @login || self.username || self.email
  end

  enum role: [:user, :seller, :customer]

  def set_default_role
    self.role ||= :user
  end

  def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["lower(phone_number) = :value OR lower(email) = :value", { :value => login.downcase }]).first
      elsif conditions.has_key?(:phone_number) || conditions.has_key?(:email)
        where(conditions.to_h).first
      end
    end

end

class Admin::BlogsController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]

  def index
    @blog= current_admin.blogs.build
    @blogs = Blog.all
  end

  def show
    @blog= Blog.find(params[:id])
  end

  def new

  end

  def create
    @blog= current_admin.blogs.build(blog_params)
    if @blog.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_blogs_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_blogs_path }
        format.js { render template: "admin/blogs/blog_error.js.erb" }
      end
    end
  end

  def edit
    @blog= Blog.find(params[:id])
  end

  def update
    @blog= Blog.find(params[:id])
    if @blog.update(blog_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_blogs_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_blogs_path }
        format.js { render template: "admin/blogs/blog_error.js.erb" }
      end
    end
  end

  def destroy
    @blog= Blog.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_blogs_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def blog_params
      params.required(:blog).permit!
    end
end

class AddFakePriceToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :fake_price, :decimal
  end
end

class Income < ApplicationRecord
  belongs_to :admin
  belongs_to :account, optional: true
  has_many :account_details

  def total_income
    sum = 0
    Income.all.each do |income|
      sum += income.amount
    end
    sum
  end

  def total_income_plus_account
    sum = 0
    Account.all.each do |account|
      sum += account.amount
      account.incomes.each do |income|
        sum = sum + income.amount
      end
    end
    sum
  end

  SELECT_CATEGORY = [
    "Work",
    "Teach"
  ]
end

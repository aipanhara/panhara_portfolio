class Expense < ApplicationRecord
  belongs_to :admin
  belongs_to :account
  
  def total_expense
    sum = 0
    Expense.all.each do |expense|
      sum += expense.amount
    end
    sum
  end

  SELECT_CATEGORY = [
    "Eletronic",
    "Food"
  ]
end

class CreateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :description
      t.string :slug
      t.boolean :status, default: true
      t.string :image
      t.integer :admin_id
      t.integer :category_id

      t.timestamps
    end
  end
end
